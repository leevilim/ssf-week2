"use strict";
import express from "express";
import multer from "multer";
import {
  getCatList,
  getCat,
  addCat,
  editCat,
  deleteCat,
} from "../controllers/catController.js";

const upload = multer({ dest: "uploads/" });
const router = express.Router();

router.get("/:id", getCat);
router.put("/:id", editCat);

router.get("/", getCatList);
router.post("/", upload.single("file"), addCat);
router.delete("/", deleteCat);

export default router;
