"use strict";
import express from "express";
import catRoute from "./routes/catRoute.js";
import userRoute from "./routes/userRoute.js";
import authRoute from "./routes/authRoute.js";
import cors from "cors";
import passport from "./utils/pass.js";
import db from "./utils/db.js";

const app = express();
const port = 3000;

app.use(cors({ origin: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use("/auth", authRoute);
app.use("/cat", passport.authenticate("jwt", { session: false }), catRoute);
app.use("/user", passport.authenticate("jwt", { session: false }), userRoute);

db.on("connected", () => {
  app.listen(port, () => {
    console.log(`app listen on port ${port}`);
  });
});
