"use strict";
import multer from "multer";
import Cat from "../data/schema/catSchema.js";
import { handleCreateOp, handleReadOp } from "../utils/try.js";
import {
  buildSearchQuery,
  buildUpdateQuery,
} from "../data/queryBuilder/catQueryBuilder.js";

const upload = multer({ dest: "uploads/" });

export const getCatList = async (req, res) => {
  handleReadOp(res, async () => {
    const query = buildSearchQuery(req);
    return await Cat.find(query);
  });
};

export const getCat = async (req, res) => {
  res.send("not implemented");
};

export const addCat = async (req, res) => {
  handleCreateOp(res, async () => {
    return await Cat.create({
      name: req.body.name,
      dateOfBirth: req.body.dateOfBirth,
      gender: req.body.gender,
      color: req.body.color,
      weight: req.body.weight,
    });
  });
};

export const editCat = async (req, res) => {
  handleCreateOp(res, async () => {
    const id = req.params.id;
    const query = buildUpdateQuery(req);
    await Cat.findByIdAndUpdate(id, query);
    return "Updated.";
  });
};

export const deleteCat = async (req, res) => {
  res.send("Delete cat");
};
