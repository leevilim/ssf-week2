"use strict";
import { users } from "../data/models/userModel.js";

export const getUserList = async (req, res) => {
  res.send(
    users.map((user) => {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
      };
    })
  );
};

export const getUser = async (req, res) => {
  const u = users.find((it) => it.id == req.params.id);
  const noPw = u
    ? {
        id: u.id,
        name: u.name,
        email: u.email,
      }
    : null;
  res.send(noPw ?? "No user with this id");
};

export const addUser = async (req, res) => {
  console.log(req.body);
  res.send("Add user");
};

export const editUser = async (req, res) => {
  res.send("Edit user");
};

export const deleteUser = async (req, res) => {
  res.send("Delete use");
};
