"use strict";
import jwt from "jsonwebtoken";
import passport from "passport";

export const login = (req, res) => {
  console.log(req.body.username);

  passport.authenticate("local", { session: false }, (err, user, info) => {
    console.log("login", info);
    if (err || !user) {
      return res.status(400).json({
        message: `authController: error ${user}`,
        user: user,
      });
    }

    req.login(user, { session: false }, (err) => {
      if (err) {
        res.send(err);
      }
      // generate a signed son web token with the contents of user object and return it in the response

      const token = jwt.sign(user, "testing");
      return res.json({ user, token });
    });
  })(req, res);
};
