export const handleCreateOp = async (res, func) => {
  try {
    const result = await func();
    res.status(201).send(result);
  } catch (error) {
    console.log("Failed createOp: ", error);
    const msg = error.message ?? "Something went wrong.";
    res.status(500).send(msg);
  }
};

export const handleReadOp = async (res, func) => {
  try {
    const result = await func();
    res.status(200).send(result);
  } catch (error) {
    console.log("Failed readOp: ", error);
    const msg = error.message ?? "Something went wrong.";
    res.status(500).send(msg);
  }
};
