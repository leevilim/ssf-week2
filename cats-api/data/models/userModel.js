"use strict";
export const users = [
  {
    id: "1",
    name: "John Doe",
    email: "john@metropolia.fi",
    password: "1234",
  },
  {
    id: "2",
    name: "Jane Doez",
    email: "jane@metropolia.fi",
    password: "qwer",
  },
];

export const getUser = (id) => {
  const user = users.find((usr) => {
    if (usr.id === id) {
      return usr;
    }
  });
  return user;
};

export const getUserLogin = (email) => {
  const user = users.find((usr) => {
    if (usr.email === email) {
      return usr;
    }
  });
  return user;
};
