import mongoose from "mongoose";

const Schema = mongoose.Schema;

const cat = new Schema({
  name: {
    type: String,
    minLength: 2,
  },
  dateOfBirth: {
    type: Date,
    max: Date.now(),
  },
  gender: {
    type: String,
    enum: ["male", "female"],
  },
  color: String,
  weight: {
    type: Number,
    max: 20,
    min: 0,
  },
});

export default mongoose.model("Cat", cat);
