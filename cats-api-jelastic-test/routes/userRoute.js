"use strict";
import express from "express";
import {
  getUser,
  addUser,
  deleteUser,
  editUser,
  getUserList,
} from "../controllers/userController.js";

const router = express.Router();

router.get("/:id", getUser);

router.get("/", getUserList);
router.post("/", addUser);
router.put("/", editUser);
router.delete("/", deleteUser);

export default router;
