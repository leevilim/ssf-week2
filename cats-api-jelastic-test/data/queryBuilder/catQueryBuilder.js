import moment from "moment";

export const buildSearchQuery = (request) => {
  const { age, weight, gender } = request.query;
  const queryObj = {};

  if (age != undefined) {
    const date = moment().subtract(age, "years").toISOString();
    console.log(date);
    queryObj.dateOfBirth = { $gt: date };
  }

  if (weight != undefined) {
    queryObj.weight = { $gt: weight };
  }

  if (gender != undefined) {
    queryObj.gender = gender;
  }

  return queryObj;
};

export const buildUpdateQuery = (request) => {
  const { name, weight, color } = request.body;
  const queryObj = {};

  if (name != undefined) {
    queryObj.name = name;
  }

  if (weight != undefined) {
    queryObj.weight = weight;
  }

  if (color != undefined) {
    queryObj.color = color;
  }

  return queryObj;
};
