"use strict";
import passport from "passport";
import passportJWT from "passport-jwt";
import { Strategy } from "passport-local";
import { getUserLogin, getUser } from "../data/models/userModel.js";

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(
  new Strategy(async (username, password, done) => {
    try {
      const user = await getUserLogin(username);
      if (user === undefined) {
        return done(null, false, { message: "Incorrect email." });
      }
      if (user.password !== password) {
        return done(null, false, { message: "Incorrect password." });
      }
      delete user.password;
      return done(null, { ...user }, { message: "Logged In Successfully" });
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "testing",
    },
    async (jwtPayload, done) => {
      try {
        const user = await getUser(jwtPayload.id);
        if (user === undefined) {
          return done(null, false);
        }
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);

export default passport;
