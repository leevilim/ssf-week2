export const handleCreateOp = async (res, func) => {
  try {
    const result = await func();
    res.status(201).json(result);
  } catch (error) {
    console.log("Failed createOp: ", error);
    const msg = error.message ?? "Could not create a new entry.";
    res.status(500).json(msg);
  }
};

export const handleReadOp = async (res, func) => {
  try {
    const result = await func();
    res.status(200).json(result);
  } catch (error) {
    console.log("Failed readOp: ", error);
    const msg = error.message ?? "Something went wrong.";
    res.status(500).json(msg);
  }
};

export const handleDeleteOp = async (res, func) => {
  try {
    const result = await func();
    res.status(204).json(result);
  } catch (error) {
    console.log("Failed deleteOp: ", error);
    const msg = error.message ?? "Deletion failed.";
    res.status(500).json(msg);
  }
};
