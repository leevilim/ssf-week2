"use strict";
import passport from "passport";
import passportJWT from "passport-jwt";
import { Strategy } from "passport-local";
import {
  getUserById,
  getUserByName,
} from "../Domain/Operations/UserOperations.js";

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(
  new Strategy(async (username, password, done) => {
    try {
      const user = await getUserByName(username);
      if (user === null) {
        return done(null, false, { message: "Incorrect username." });
      }
      if (user.password !== password) {
        return done(null, false, { message: "Incorrect password." });
      }
      delete user.password;
      delete user.__v;
      return done(null, { ...user }, { message: "Logged In Successfully" });
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "testing",
    },
    async (jwtPayload, done) => {
      try {
        const user = await getUserById(jwtPayload._id);
        if (user === null) {
          return done(null, false);
        }
        delete user.password;
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);

export default passport;
