export const jsonOptions = {
  toJSON: {
    transform: (doc, ret) => {
      delete ret.__v;
    },
  },
};
