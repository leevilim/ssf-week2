import express from "express";
import db from "./Utils/Db.js";
import cors from "cors";
import passport from "./utils/pass.js";
import stationRoute from "./Route/StationRoute.js";
import authRoute from "./Route/AuthRoute.js";

import a from "./Domain/Schema/Connection.js";
import b from "./Domain/Schema/ConnectionType.js";
import c from "./Domain/Schema/CurrentType.js";
import d from "./Domain/Schema/Level.js";
import e from "./Domain/Schema/Station.js";
import f from "./Domain/Schema/User.js";

const app = express();
const port = process.env.PORT || 3000;

app.use(cors({ origin: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use("/auth", authRoute);
app.use(
  "/station",
  passport.authenticate("jwt", { session: false }),
  stationRoute
);

db.on("connected", () => {
  app.listen(port, () => {
    console.log(`app listen on port ${port}`);
  });
});
