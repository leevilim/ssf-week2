# Example requests

# Login

/auth/login - POST

{
"username": [your_username],
"password": [your_password]
}
(db contains the users from the mongodump provided)

# Create station

/station - POST

{
"Station": {
"Title": "Test123",
"Town": "Espoo",
"AddressLine1": "UUS 8b",
"StateOrProvince": "Southern Finland",
"Postcode": "02630",
"Location": {
"coordinates": [24.77772323548868, 60.203353130088146]
}
},
"Connections":[
{
"ConnectionTypeID": "5e39eecac5598269fdad81a0",
"CurrentTypeID": "5e39ef4a6921476aaf62404a",
"LevelID": "5e39edf7bb7ae768f05cf2bc",
"Quantity": 2
}
]
}

# Get single station

/station/623f321749bab6792a21bbb4 - GET

# Update station

/station - PUT

{
"Station": {
"\_id": "623f321749bab6792a21bbb4",
"Title": "Test update",
"Town": "Espoo",
"AddressLine1": "Sinimäentie 8b",
"StateOrProvince": "Southern Finland",
"Postcode": "02630",
"Location": {
"coordinates": [24.77772323548868, 60.203353130088146]
}
},
"Connections":[
{
"_id": "5e3a02368637aa01278b6806",
"ConnectionTypeID": "5e39eecac5598269fdad81a0",
"CurrentTypeID": "5e39ef4a6921476aaf62404a",
"LevelID": "5e39edf7bb7ae768f05cf2bc",
"Quantity": 7
}
]
}

# Delete station

/station/[stationId] - DELETE

# Query stations with params

Params:

- Query with location, provide both bottomLeft + topRight: {lat: x, lng: y}
- Limit res count: limit=x
- Get next results for limited queries: start=x

/station?topRight={"lat":60.2821946,"lng":25.036108}&bottomLeft={"lat":60.1552076,"lng":24.7816538}&limit=2 - GET
