import {
  getStationById,
  queryStations,
  createStation,
  updateStation,
  deleteStationById,
} from "./../Domain/Operations/StationOperations.js";
import { handleCreateOp, handleReadOp, handleDeleteOp } from "../Utils/Try.js";
import { rectangleBounds } from "../Utils/Other.js";

export const getStation = async (req, res) => {
  handleReadOp(res, async () => await getStationById(req.params.id));
};

export const getStations = async (req, res) => {
  handleReadOp(
    res,
    async () => await queryStations(buildQuery(req), buildOptions(req))
  );
};

export const addStation = async (req, res) => {
  handleCreateOp(res, async () => await createStation(req.body));
};

export const editStation = async (req, res) => {
  handleCreateOp(res, async () => await updateStation(req.body));
};

export const deleteStation = async (req, res) => {
  handleDeleteOp(res, async () => await deleteStationById(req.params.id));
};

// ................

const buildQuery = (req) => {
  const query = {};
  const containsCoords =
    req.query.topRight !== undefined && req.query.bottomLeft !== null;

  if (containsCoords) {
    query.Location = {
      $geoWithin: {
        $geometry: rectangleBounds(req.query.topRight, req.query.bottomLeft),
      },
    };
  }
};

const buildOptions = (req) => {
  return {
    limit: req.query.limit || 10,
    skip: req.query.start || 0,
  };
};
