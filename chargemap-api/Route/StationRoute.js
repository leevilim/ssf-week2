import express from "express";
import {
  getStation,
  getStations,
  addStation,
  editStation,
  deleteStation,
} from "./../Controller/StationController.js";

const router = express.Router();

router.get("/", getStations);
router.post("/", addStation);
router.put("/", editStation);

router.get("/:id", getStation);
router.delete("/:id", deleteStation);

export default router;
