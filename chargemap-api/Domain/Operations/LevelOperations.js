import Level from "../Schema/Level.js";

export const getLevel = async (id) => {
  return await Level.findById(id);
};
