import ConnectionType from "../Schema/ConnectionType.js";

export const getConnectionType = async (id) => {
  return await ConnectionType.findById(id);
};
