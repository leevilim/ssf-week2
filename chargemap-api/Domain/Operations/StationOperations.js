import Station from "../Schema/Station.js";
import { renameKey } from "../../Utils/Other.js";
import {
  createConnections,
  updateOrCreateConnections,
} from "./ConnectionOperations.js";
import mongoose from "mongoose";

export const getStationById = async (id) => {
  return await Station.findById(id)
    .populate({
      path: "Connections",
      select: "-_id -__v",
      populate: {
        path: "LevelID ConnectionTypeID CurrentTypeID",
        select: "FormalName Title Comments IsFastChargeCapable Description",
      },
    })
    .select("-__v")
    .transform((doc) => removeIDFromDocumentKeys(doc));
};

export const queryStations = async (query = {}, options = {}) => {
  return await Station.find(query)
    .setOptions(options)
    .populate({
      path: "Connections",
      select: "-_id -__v",
      populate: {
        path: "LevelID ConnectionTypeID CurrentTypeID",
        select: "FormalName Title Comments IsFastChargeCapable Description",
      },
    })
    .select("-__v")
    .transform((docs) => docs.map((it) => removeIDFromDocumentKeys(it)));
};

export const createStation = async (data) => {
  const connectionIds = await createConnections(data.Connections);
  const station = data.Station;

  const createRes = await Station.create({
    _id: mongoose.Types.ObjectId(),
    Location: {
      type: "Point",
      coordinates: station.Location.coordinates,
    },
    Connections: connectionIds,
    Title: station.Title,
    AddressLine1: station.AddressLine1,
    Town: station.Town,
    StateOrProvince: station.StateOrProvince,
    Postcode: station.Postcode,
  });

  return createRes._id;
};

export const updateStation = async (data) => {
  const connectionIds = await updateOrCreateConnections(data.Connections);
  const station = data.Station;

  const updateRes = await Station.findByIdAndUpdate(station._id, {
    Location: {
      type: "Point",
      coordinates: station.Location.coordinates,
    },
    Connections: connectionIds,
    Title: station.Title,
    AddressLine1: station.AddressLine1,
    Town: station.Town,
    StateOrProvince: station.StateOrProvince,
    Postcode: station.Postcode,
  });

  return updateRes._id;
};

export const deleteStationById = async (id) => {
  await Station.findByIdAndDelete(id);
  return "Deleted.";
};

const removeIDFromDocumentKeys = (document) => {
  if (document == null) {
    return "Document not found.";
  }
  const json = document.toJSON();

  Object.values(json.Connections).forEach((connection) => {
    Object.keys(connection).forEach((key) => {
      const newKey = key.replace("ID", "");
      renameKey(connection, key, newKey);
    });
  });
  return json;
};
