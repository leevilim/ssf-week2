import Connection from "../Schema/Connection.js";
import mongoose from "mongoose";

/**
 * Pass an array of connections.
 * If the connection contains an id, modifies that existing connection.
 * If id not provided, creates a new connection.
 * @return string[], all the connection ids touched
 */
export const updateOrCreateConnections = async (connectionArray) => {
  const connectionIds = await Promise.all(
    connectionArray.map(async (connection) => {
      const containsId = connection._id !== undefined;
      if (containsId) {
        return await updateConnection(connection);
      }

      return await createConnection(connection);
    })
  );

  return connectionIds;
};

/** Create connections. Return an array of the created connection ids */
export const createConnections = async (connectionArray) => {
  const connectionIds = await Promise.all(
    connectionArray.map(async (connection) => {
      const connectionId = await createConnection(connection);
      return connectionId;
    })
  );

  return connectionIds;
};

/** Create connection. Returns the id of the created connection. */
const createConnection = async (data) => {
  const createRes = await Connection.create({
    _id: new mongoose.Types.ObjectId(),
    ConnectionTypeID: data.ConnectionTypeID,
    LevelID: data.LevelID,
    CurrentTypeID: data.CurrentTypeID,
    Quantity: data.Quantity,
  });

  return createRes._id;
};

const updateConnection = async (data) => {
  const updateRes = await Connection.findByIdAndUpdate(data._id, {
    ConnectionTypeID: data.ConnectionTypeID,
    LevelID: data.LevelID,
    CurrentTypeID: data.CurrentTypeID,
    Quantity: data.Quantity,
  });
  return updateRes._id;
};
