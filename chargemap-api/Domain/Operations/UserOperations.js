import User from "../Schema/User.js";

export const getUserByName = async (username) => {
  const user = await User.find({ username: username }).lean();
  return user[0] ?? null;
};

export const getUserById = async (id) => {
  const user = await User.findById(id);

  return user ?? null;
};
