import CurrentType from "../Schema/CurrentType.js";

export const getCurrentType = async (id) => {
  return await CurrentType.findById(id);
};
