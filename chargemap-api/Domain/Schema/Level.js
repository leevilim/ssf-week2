import mongoose from "mongoose";
import { jsonOptions } from "../../Utils/SchemaOptions.js";

const levelSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.ObjectId,
    Comments: String,
    IsFastChargeCapable: Boolean,
  },
  jsonOptions
);

export default mongoose.model("Level", levelSchema);
