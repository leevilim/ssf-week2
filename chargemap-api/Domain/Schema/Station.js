import mongoose from "mongoose";
import { jsonOptions } from "../../Utils/SchemaOptions.js";

const stationSchema = new mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  Location: {
    type: {
      type: String,
      enum: ["Point"],
      required: true,
    },
    coordinates: {
      type: [Number],
      required: true,
    },
  },
  Connections: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Connection",
    },
  ],
  Title: String,
  AddressLine1: String,
  Town: String,
  StateOrProvince: String,
  Postcode: String,
});

stationSchema.index({ Location: "2dsphere" });

export default mongoose.model("Station", stationSchema);
